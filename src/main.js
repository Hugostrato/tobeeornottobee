import Vue from 'vue'

import VueRouter from "vue-router";

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [{
    path: '/',
    component: require('./components/HomePage.vue').default
  }, {
    path: '/admin',
    component: require('./components/Admin.vue').default
  }, {
    path: '/configuration',
    component: require('./components/Configuration.vue').default
  },{
    path: '/ruchers',
    component: require('./components/MesRuchers.vue').default
  },{
    path: '/ajoutrucher',
    component: require('./components/AjoutRucher.vue').default
  },{
    path: '/listeRuchers',
    component: require('./components/ListeRucher.vue').default
  },{
    path: '/carteRuchers',
    component: require('./components/CarteRucher.vue').default
  },{
    path: '/visiteRucher',
    component: require('./components/VisiteRucher.vue').default
  },{
    path: '*',
    redirect: '/'
  }]
})

import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  render: h => h(App),
}).$mount('#app')
